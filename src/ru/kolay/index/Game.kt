package ru.kolay.index

import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement

open class Game(protected val canvas: HTMLCanvasElement) {
    protected open val canvasH = 500
    protected open val canvasW = 1000
    protected val ctx = canvas.getContext("2d") as CanvasRenderingContext2D
    open fun start() {
        canvas.tabIndex = 1
        canvas.height = canvasH
        canvas.width = canvasW
        canvas.focus()
    }
}