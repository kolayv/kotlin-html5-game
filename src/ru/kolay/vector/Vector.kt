package ru.kolay.vector

open class Vector(open var x: Double = 0.toDouble()) {
    fun equals(other: Vector): Boolean {
        return other.x == x
    }
}
