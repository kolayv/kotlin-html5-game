package ru.kolay.vector

data class Vector2(override var x: Double = 0.toDouble(), var y: Double = 0.toDouble()): Vector()
