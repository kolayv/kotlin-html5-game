@file:Suppress("DEPRECATION")

package ru.kolay.snake

import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.HTMLParagraphElement
import org.w3c.dom.events.KeyboardEvent
import ru.kolay.index.Game
import ru.kolay.vector.Vector2
import kotlin.browser.document
import kotlin.browser.window
import kotlin.js.Math

class Game(canvas: HTMLCanvasElement) : Game(canvas) {
    private val cell = 20.toDouble()
    private val fps = 70
    private var snake = mutableListOf(Vector2())
    private var direction = Directions.RIGHT
    private var loopID: Int? = null
    private var food = getFood()
    private val scoreP = document.getElementById("score") as HTMLParagraphElement
    private var score = 0
    override val canvasH = 500
    override val canvasW = 500

    private fun newGame() {
        if (loopID != null) {
            window.clearInterval(loopID!!)
            loopID = null
            return newGame()
        }
        food = getFood()
        score = 0
        snake = mutableListOf(Vector2())
        direction = Directions.RIGHT
        loopID = window.setInterval({loop()}, fps)
    }

    override fun start() {
        super.start()
        canvas.onkeydown = {event ->
            event as KeyboardEvent
            val newDirection = getDirection(event.keyCode)
            if (newDirection != null)
                if (newDirection.reverse() != direction)
                    direction = newDirection
        }
        newGame()
    }

    private fun loop() {
        val pos = snake[0].copy()
        when (direction) {
            Directions.LEFT -> pos.x -= cell
            Directions.UP -> pos.y -= cell
            Directions.RIGHT -> pos.x += cell
            Directions.DOWN -> pos.y += cell
        }
        if (snake[0] == food) {
            food = getFood()
            score++
        }
        else
            snake.removeAt(snake.lastIndex)
        if (pos.x < 0) {
            pos.x = canvasH.toDouble()
        }
        else if (pos.x > canvasH) {
            pos.x = 0.0
        }
        if (pos.y < 0) {
            pos.y = canvasW.toDouble()
        }
        else if (pos.y > canvasW) {
            pos.y = 0.0
        }
        if (pos in snake)
            return newGame()
        snake = (mutableListOf(pos) + snake).toMutableList()
        redraw()
    }

    private fun getFood(): Vector2 {
        val x = Math.floor(Math.random()*canvasH / cell) * cell
        val y = Math.floor(Math.random()*canvasW / cell) * cell
        return Vector2(x, y)
    }

    private fun redraw() {
        scoreP.innerText = "Score: $score"
        ctx.fillStyle = "#D3D3D3"
        ctx.fillRect(0.toDouble(), 0.toDouble(), canvasW.toDouble(), canvasH.toDouble())
        ctx.fillStyle = "#FF0000"
        ctx.fillRect(food.x, food.y, cell, cell)
        ctx.fillStyle = "#000000"
        for (pos in snake)
            ctx.fillRect(pos.x, pos.y, cell, cell)
    }

    enum class Directions (val keyCode: Int) {
        LEFT(37),
        UP(38),
        RIGHT(39),
        DOWN(40);
        fun reverse() = when (this) {
            RIGHT -> LEFT
            LEFT -> RIGHT
            UP -> DOWN
            DOWN -> UP
        }
    }

    private fun getDirection(keyCode: Int) = when (keyCode) {
        Directions.RIGHT.keyCode -> Directions.RIGHT
        Directions.LEFT.keyCode -> Directions.LEFT
        Directions.UP.keyCode -> Directions.UP
        Directions.DOWN.keyCode -> Directions.DOWN
        else -> null
    }
}