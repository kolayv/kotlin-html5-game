package ru.kolay.snake

import org.w3c.dom.HTMLCanvasElement
import kotlin.browser.document


fun main(args: Array<String>) {
    val canvas = document.getElementById("canvas") as HTMLCanvasElement
    Game(canvas).start()
}
